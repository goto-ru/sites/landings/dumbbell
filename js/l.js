/*! Forvo v1.0.0 https://forvo.com | Copyright (c) 2016 - Forvo Media https://forvo.com */

function loadSelectMenu() {
    myJQuery("form.form select, .ui-select").selectmenu({
        change: function (a, b) {
            myJQuery(this).trigger("change")
        }, open: function (a, b) {
            myJQuery(this).trigger("click")
        }
    })
}
function hasFlashPlugin() {
    function a(a) {
        try {
            return new ActiveXObject(a), !0
        } catch (b) {
            return !1
        }
    }

    for (var b = 0; b < navigator.plugins.length; b++)if (navigator.plugins[b].name.toLowerCase().indexOf("flash") > -1)return !0;
    return a("ShockwaveFlash.ShockwaveFlash")
}
function Play(a, b, c, d, e, f, g) {
    if (_SERVER_HOST == _AUDIO_HTTP_HOST) {
        var b = defaultProtocol + "//" + _SERVER_HOST + "/player-mp3Handler.php?path=" + b, c = defaultProtocol + "//" + _SERVER_HOST + "/player-oggHandler.php?path=" + c;
        if ("undefined" != typeof e && void 0 !== e && null !== e && "" !== e)var e = defaultProtocol + "//" + _SERVER_HOST + "/player-mp3-highHandler.php?path=" + e; else var e = "";
        if ("undefined" != typeof f && void 0 !== f && null !== f && "" !== f)var f = defaultProtocol + "//" + _SERVER_HOST + "/player-ogg-highHandler.php?path=" + f; else var f = ""
    } else {
        var b = defaultProtocol + "//" + _AUDIO_HTTP_HOST + "/mp3/" + base64_decode(b), c = defaultProtocol + "//" + _AUDIO_HTTP_HOST + "/ogg/" + base64_decode(c);
        if ("undefined" != typeof e && void 0 !== e && null !== e && "" !== e)var e = defaultProtocol + "//" + _AUDIO_HTTP_HOST + "/audios/mp3/" + base64_decode(e); else var e = "";
        if ("undefined" != typeof f && void 0 !== f && null !== f && "" !== f)var f = defaultProtocol + "//" + _AUDIO_HTTP_HOST + "/audios/ogg/" + base64_decode(f); else var f = ""
    }
    if ("undefined" == typeof g || void 0 == g || null == g || "" == g)var g = "l";
    var h = !!document.createElement("audio").canPlayType;
    if (d = d ? !0 : !1, h) {
        var i = navigator.userAgent.toLowerCase(), j = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(i);
        createAudioObject(a, b, c, j, d, e, f, g)
    } else {
        var k = '<object type="application/x-shockwave-flash" data="' + player_path + '" width="1" height="1"><param name="movie" value="' + player_path + '" /><param name="flashvars" value="path=' + b + "&amp;_SERVER_HTTP_HOST=" + _SERVER_HOST + '" /></object>', l = document.getElementById("player");
        l.innerHTML = k
    }
    return isNaN(a) && -1 != a.indexOf("_map") && (a = a.split("_", 1)), sumHit(a), ga("send", "event", "Play", "Web", a), !0
}
function PlayPhrase(a, b, c, d) {
    if (_SERVER_HOST == _AUDIO_HTTP_HOST)var b = defaultProtocol + "//" + _SERVER_HOST + "/player-phrasesMp3Handler.php?path=" + b, c = defaultProtocol + "//" + _SERVER_HOST + "/player-phrasesOggHandler.php?path=" + c; else var b = defaultProtocol + "//" + _AUDIO_HTTP_HOST + "/phrases/mp3/" + base64_decode(b), c = defaultProtocol + "//" + _AUDIO_HTTP_HOST + "/phrases/ogg/" + base64_decode(c);
    var e = !!document.createElement("audio").canPlayType;
    if (d = d ? !0 : !1, e) {
        var f = navigator.userAgent.toLowerCase(), g = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(f);
        createAudioObject(a, b, c, g, d, "", "", "")
    } else {
        var h = '<object type="application/x-shockwave-flash" data="' + player_path + '" width="1" height="1"><param name="movie" value="' + player_path + '" /><param name="flashvars" value="path=' + b + "&amp;_SERVER_HTTP_HOST=" + _SERVER_HOST + '" /></object>', i = document.getElementById("player");
        i.innerHTML = h
    }
    return !0
}
function createAudioObject(a, b, c, d, e, f, g, h) {
    var i = document.createElement("audio");
    if ("undefined" != typeof h && void 0 !== h && null !== h && "" !== h && "h" == h) {
        if ("undefined" != typeof f && void 0 !== f && null !== f && "" !== f) {
            var j = document.createElement("source");
            j.type = "audio/mp3", j.src = f, i.appendChild(j)
        }
        if ("undefined" != typeof g && void 0 !== g && null !== g && "" !== g) {
            var k = document.createElement("source");
            k.type = "audio/ogg", k.src = g, i.appendChild(k)
        }
    }
    if (null !== b) {
        var l = document.createElement("source");
        l.type = "audio/mp3", l.src = b, i.appendChild(l)
    }
    if (null !== c) {
        var m = document.createElement("source");
        m.type = "audio/ogg", m.src = c, i.appendChild(m)
    }
    if (d || (i.autoplay = !0), e) {
        var n = function (a) {
            var b = a.target.src, c = window.location.href;
            reportMissingAudioFile(b, c)
        };
        i.addEventListener("error", n, !0)
    } else {
        var o = $("#play_" + a), p = function () {
            o.removeClass("error"), o.addClass("loading")
        }, q = function () {
            o.removeClass("error"), o.removeClass("loading"), o.addClass("playing")
        }, n = function (a) {
            o.removeClass("loading"), o.removeClass("playing"), o.addClass("error");
            var b = a.target.src, c = window.location.href;
            reportMissingAudioFile(b, c)
        }, r = function () {
            o.removeClass("loading"), o.removeClass("playing")
        };
        i.addEventListener("loadstart", p, !1), i.addEventListener("play", q, !1), i.addEventListener("error", n, !0), i.addEventListener("ended", r, !1)
    }
    d && i.play()
}
function reportMissingAudioFile(a, b) {
    var c = "/reportMissingAudioFile.php";
    $.ajax({
        type: "POST", data: {target: a, location: b}, url: c, success: function (a) {
        }
    })
}
function base64_decode(a) {
    var b, c, d, e, f, g, h, i, j = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", k = ac = 0, l = "", m = [];
    if (!a)return a;
    a += "";
    do e = j.indexOf(a.charAt(k++)), f = j.indexOf(a.charAt(k++)), g = j.indexOf(a.charAt(k++)), h = j.indexOf(a.charAt(k++)), i = e << 18 | f << 12 | g << 6 | h, b = i >> 16 & 255, c = i >> 8 & 255, d = 255 & i, 64 == g ? m[ac++] = String.fromCharCode(b) : 64 == h ? m[ac++] = String.fromCharCode(b, c) : m[ac++] = String.fromCharCode(b, c, d); while (k < a.length);
    return l = m.join(""), l = utf8_decode(l)
}
function utf8_decode(a) {
    var b = [], c = 0, d = 0, e = 0, f = 0, g = 0;
    for (a += ""; c < a.length;)e = a.charCodeAt(c), 128 > e ? (b[d++] = String.fromCharCode(e), c++) : e > 191 && 224 > e ? (f = a.charCodeAt(c + 1), b[d++] = String.fromCharCode((31 & e) << 6 | 63 & f), c += 2) : (f = a.charCodeAt(c + 1), g = a.charCodeAt(c + 2), b[d++] = String.fromCharCode((15 & e) << 12 | (63 & f) << 6 | 63 & g), c += 3);
    return b.join("")
}
function sumHit(a) {
    return "translate" == location.hostname.split(".")[0] ? !0 : ($.get("/sum-hit/" + a), !0)
}
function control_messages_mail(a) {
    document.getElementById("allow_user_messages_mail_container").className = a ? "indent_on" : "indent_off", document.getElementById("allow_user_messages_mail").disabled = !a
}
function fpopup() {
    "undefined" != typeof fntcpopup && void 0 !== fntcpopup && null !== fntcpopup && "1" == fntcpopup && $.magnificPopup.open({
        items: {src: "foo.jpg"},
        type: "ajax",
        ajax: {settings: {url: "/fntc-mfp.php"}},
        mainClass: "mfp_page_phrases-fntc",
        closeOnBgClick: !1,
        showCloseBtn: !0
    }, 0)
}
!function () {
    for (var a, b = function () {
    }, c = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeline", "timelineEnd", "timeStamp", "trace", "warn"], d = c.length, e = window.console = window.console || {}; d--;)a = c[d], e[a] || (e[a] = b)
}(), function (a) {
    a.fn.extend({
        softScroll: function (b) {
            var c = {duration: 600, effect: "swing"}, b = a.extend(c, b);
            return this.each(function () {
                a(this).on("click", function (c) {
                    c.preventDefault();
                    var d = this.hash, e = a(d);
                    a("html, body").stop().animate({scrollTop: e.offset().top}, parseInt(b.duration), b.effect, function () {
                        window.location.hash = d
                    })
                })
            })
        }
    })
}(jQuery), function (a) {
    a.fn.extend({
        countChars: function (b) {
            function c(a, c, e, f, g) {
                var h = a.val().length;
                return h = d(h, g), h > f ? c.addClass(b.overflow_class) : c.removeClass(b.overflow_class), e.html(h), !0
            }

            function d(a, b) {
                a = a.toString();
                for (var c = [], d = a.length; d > 0; d -= 3)c.push(a.substring(d - 3, d));
                return c.reverse(), c.join(b)
            }

            var e = {
                obj_counter_sibling_selector: ".counter",
                obj_counter_chars_selector: ".counter_chars",
                overflow_class: "error",
                thousand_char: null
            }, b = a.extend(e, b);
            return this.each(function () {
                var d = a(this), e = a(this).siblings(b.obj_counter_sibling_selector), f = e.find(b.obj_counter_chars_selector), g = d.prop("maxlength"), h = null == b.thousand_char ? d.data("thousand-char") : "";
                c(d, e, f, g, h), d.keyup(function () {
                    c(d, e, f, g, h)
                })
            })
        }
    })
}(jQuery);
var myJQuery;
jQuery(function (a) {
    myJQuery = a, loadSelectMenu()
}), jQuery(function (a) {
    a("#wrap_main_nav_and_search .trigger_menu").click(function () {
        return a("#nav_main").slideToggle(), !1
    })
}), jQuery(function (a) {
    var b = a("#nav_user.logged > ul");
    b.find(".close").length || (b.prepend('<li class="username">' + a("#nav_user .welcome strong").html() + "</li>"), b.append('<li class="close">&nbsp;</li>')), a("#nav_user p.welcome, #nav_user.logged .close").click(function () {
        return "fixed" == b.css("position") && (b.toggleClass("active"), a("body").toggleClass("modal")), !1
    })
}), jQuery(function (a) {
    a("a[href^='#']").each(function () {
        this.href = location.href.split("#")[0] + "#" + this.href.substr(this.href.indexOf("#") + 1)
    })
}), jQuery(function (a) {
    jQuery().softScroll && a("a.softScroll, .nav_langs a").each(function () {
        this.href.indexOf("#") > -1 && !a(this).hasClass("noSoftScroll") && a(this).softScroll({duration: 600})
    })
}), jQuery(function (a) {
    hasFlashPlugin() ? a("html").first().addClass("flash") : a("html").first().addClass("noflash")
}), jQuery(function (a) {
    a(".focus").focus()
}), jQuery(function (a) {
    var b = a("#cookie_assistant"), c = b.find(".accept");
    c.click(function () {
        a(this).prop("href");
        return a.ajax({
            type: "POST", data: {close: !0}, url: a(this).prop("href"), success: function (a) {
                var c = b.innerHeight();
                b.css("bottom", -c), setTimeout(function () {
                    b.remove()
                }, 500)
            }
        }), !1
    })
}), jQuery(function (a) {
    jQuery().tagit && a(".inputMultiple").tagit({
        autocomplete: {
            source: function (b, c) {
                var d = (b.term.toLowerCase(), this);
                a.ajax({
                    type: "GET",
                    url: this.element.data("url-source"),
                    dataType: "json",
                    data: {
                        q: b.term, id_word: id_word, allTags: function () {
                            var a = d.element.val();
                            return a.length && "," != a.charAt(a.length - 1) && (a += ","), a
                        }
                    },
                    success: function (b) {
                        c(a.grep(b, function (a) {
                            return a
                        }))
                    }
                })
            }
        }, singleField: !0, singleFieldNode: this.element, allowSpaces: !0
    })
}), jQuery(function (a) {
    a(".focus").first().focus()
}), jQuery(function (a) {
    a("form.form input.checkall").click(function () {
        var b = a(this).is(":checked"), c = a(this).closest("form"), d = c.find("input[type=checkbox]");
        d.prop("checked", b)
    })
}), jQuery(function (a) {
    a(".countChars").countChars()
}), jQuery(function (a) {
    a(".tabs").tabs()
}), jQuery(function (a) {
    var b = a(".changedir");
    b.length && b.each(function () {
        a(this).click(function () {
            var b = a(this), c = a(b.data("target")), d = b.data("text-ltr"), e = b.data("text-rtl");
            return "ltr" == c.prop("dir") ? (c.prop("dir", "rtl"), b.html(e)) : (c.prop("dir", "ltr"), b.html(d)), c.focus(), !1
        })
    })
}), jQuery(function (a) {
    var b = a("#interfaz_other_languages_trigger"), c = a("#footer_other_languages_list");
    b.click(function () {
        return c.slideToggle("fast"), !1
    })
}), jQuery(function (a) {
    var b = a("#word_search_header"), c = a("#id_lang");
    if (c.length) {
        var d = "", e = "/searchs-ajax-load?lang=" + d;
        c.change(function () {
            d = c.find("option:selected").val(), e = "/searchs-ajax-load?lang=" + d, b.autocomplete("option", "source", e)
        })
    } else var e = "/searchs-ajax-load";
    b.autocomplete({
        source: e, open: function (b, c) {
            a(this).autocomplete("widget").css({})
        }, select: function (b, c) {
            b.target.value = c.item.value;
            var d = a(b.target).closest("form");
            return document.getElementById(d.attr("id")).submit(), !1
        }
    }).focus(function () {
        a(this).autocomplete("search")
    }), a("#language_search_header").selectmenu({
        change: function (b, c) {
            a(this).trigger("change")
        }
    });
    var f = a("#search"), g = f.find("nav li");
    g.each(function (b) {
        a(this).hasClass(f.prop("class")) && a(this).addClass("active")
    }), g.filter(".active").length || g.first().addClass("active"), g.find("a").click(function () {
        return f.removeClass("classic new").addClass(a(this).closest("li").prop("class")), g.removeClass("active"), a(this).closest("li").addClass("active"), "search_language" == a(this).attr("id") ? a("#language_search_header").prop("disabled", !1) : a("#language_search_header").prop("disabled", !0), !1
    })
}), jQuery(function (a) {
    a("#chat_options").hide(), a("#chat-trigger").click(function () {
        return a("#chat_options").slideToggle(), a("#chat-trigger").toggleClass("bg"), !1
    })
}), jQuery(function (a) {
    var b = a("#div-gpt-ad-1435318051797-6"), c = (b.offset(), a(window).height());
    a(window).scroll(function () {
        a(window).scrollTop() > c && "static" == b.css("position") ? b.addClass("fixed") : a(window).scrollTop() < c && b.hasClass("fixed") && b.removeClass("fixed")
    })
}), jQuery(function (a) {
    function b(b, c) {
        a.ajax({
            type: "POST", dataType: "json", data: {action: b, data: c}, url: "/lst/", success: function (b) {
                1 == b.error && console.warn(b.errorMsg), a.magnificPopup.close()
            }
        })
    }

    a(document).on("click", "#show_student", function () {
        a(this).is(":checked") && (a("#teacher_language").hide(), a("#student_language").show())
    }), a(document).on("click", "#show_teacher", function () {
        a(this).is(":checked") && (a("#student_language").hide(), a("#teacher_language").show())
    }), a(document).on("click", "#lstSave", function (c) {
        var d = {}, e = "", f = "";
        a("#show_student").is(":checked") && (e = a("#show_student").val(), f = a("#id_lang_student").val()), a("#show_teacher").is(":checked") && (e = a("#show_teacher").val(), f = a("#id_lang_teacher").val()), d.teacher = e, d.lang = f, b("save", d)
    }), a(document).on("click", "#lstRemind", function (a) {
        b("remind", "")
    }), a(document).on("click", "#lstNever", function (a) {
        b("never", "")
    }), "undefined" != typeof lst && void 0 !== lst && null !== lst && "1" == lst && a.magnificPopup.open({
        items: {src: "foo.jpg"},
        type: "ajax",
        ajax: {settings: {url: "/lst.php"}},
        mainClass: "mfp_page_student_teacher-mfp",
        closeOnBgClick: !1,
        showCloseBtn: !0,
        callbacks: {
            ajaxContentAdded: function () {
                loadSelectMenu()
            }
        }
    }, 0)
}), jQuery(function (a) {
    "undefined" != typeof fixMenuMobile && void 0 !== fixMenuMobile && null !== fixMenuMobile && "1" == fixMenuMobile && a(window).scroll(function () {
        var b = a("#nav_common"), c = a(window).scrollTop();
        c >= 55 ? b.addClass("fixed") : b.removeClass("fixed")
    })
});