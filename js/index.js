var now_slide = 0;
var slides = [".slide0", ".slide1", ".slide2", ".slide3", ".slide4"];

function go_to_slide(n) {
    if (n < 0 || n > 4) {
        return
    }

    if (n==4){
        $("#ss_menu").css("display","none");
    } else {
        $("#ss_menu").css("display","block");
    }



    if (n > now_slide) {
        $(slides[now_slide] + "_button").css("text-decoration", "none");
        while (n > now_slide) {
            $(slides[now_slide]).animate({
                left: "-100vw"
            });

            now_slide += 1;
        }
        $(slides[now_slide]).animate({
            left: "0",
            right: "100vw"
        });


        $(slides[now_slide] + "_button").css("text-decoration", "underline");
    }
    else if (n < now_slide) {
        $(slides[now_slide] + "_button").css("text-decoration", "none");
        while (n < now_slide) {
            $(slides[now_slide]).animate({
                left: "100vw",
                right: "0"
            });
            now_slide -= 1
        }

        $(slides[now_slide]).animate({
            left: "0"
        });
        $(slides[now_slide] + "_button").css("text-decoration", "underline");
    }
}


$(document).keydown(function (e) {
    if (e.keyCode == 39) {
        go_to_slide(now_slide + 1);
        e.preventDefault();
    }

    else if (e.keyCode == 37) {
        go_to_slide(now_slide - 1);
        e.preventDefault();
    }
});

jQuery(document).ready(function ($) {

    var jssor_1_SlideoTransitions = [
        [{b: -1, d: 1, o: -1}, {b: 0, d: 1000, o: 1}],
        [{b: 1900, d: 2000, x: -379, e: {x: 7}}],
        [{b: 1900, d: 2000, x: -379, e: {x: 7}}],
        [{b: -1, d: 1, o: -1, r: 288, sX: 9, sY: 9}, {
            b: 1000,
            d: 900,
            x: -1400,
            y: -660,
            o: 1,
            r: -288,
            sX: -9,
            sY: -9,
            e: {r: 6}
        }, {b: 1900, d: 1600, x: -200, o: -1, e: {x: 16}}]
    ];

    var jssor_1_options = {
        $AutoPlay: true,
        $SlideDuration: 800,
        $SlideEasing: $Jease$.$OutQuint,
        $CaptionSliderOptions: {
            $Class: $JssorCaptionSlideo$,
            $Transitions: jssor_1_SlideoTransitions
        },
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$
        }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    /*responsive code begin*/
    /*you can remove responsive code if you don't want the slider scales while window resizing*/
    function ScaleSlider() {
        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
        if (refSize) {
            refSize = Math.min(refSize, 1920);
            jssor_1_slider.$ScaleWidth(refSize);
        }
        else {
            window.setTimeout(ScaleSlider, 30);
        }
    }

    ScaleSlider();
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    /*responsive code end*/
});


$(document).ready(function (ev) {
    var toggle = $('#ss_toggle');
    var menu = $('#ss_menu');
    var rot;

    $('#ss_toggle').on('click', function (ev) {
        rot = parseInt($(this).data('rot')) - 180;
        menu.css('transform', 'rotate(' + rot + 'deg)');
        menu.css('webkitTransform', 'rotate(' + rot + 'deg)');
        if ((rot / 180) % 2 == 0) {
            //Moving in
            toggle.parent().addClass('ss_active');
            toggle.addClass('close');
        } else {
            //Moving Out
            toggle.parent().removeClass('ss_active');
            toggle.removeClass('close');
        }
        $(this).data('rot', rot);
    });

    menu.on('transitionend webkitTransitionEnd oTransitionEnd', function () {
        if ((rot / 180) % 2 == 0) {
            $('#ss_menu div i').addClass('ss_animate');
        } else {
            $('#ss_menu div i').removeClass('ss_animate');
        }
    });

});